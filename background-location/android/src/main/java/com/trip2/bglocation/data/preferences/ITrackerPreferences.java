package com.trip2.bglocation.data.preferences;

import com.trip2.bglocation.entities.SessionData;

import org.json.JSONException;
import org.json.JSONObject;

public interface ITrackerPreferences {

    void save(SessionData data);

    void setDriverStatus(JSONObject driverStatus);

    JSONObject getDriverStatus() throws JSONException;

    void setTripsIds(String tripsIds);

    String getTripsIds();

    void clearData();

    SessionData getSessionData();
}
